package com.ericsson.iot.api.cm.boundry.external;

import com.ericsson.iot.api.cm.control.PersonForm;
import com.ericsson.iot.api.cm.control.model.entity.Person;
import com.ericsson.iot.api.cm.control.model.service.PersonService;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/iot/api/person")
@RequestScoped
public class PersonResource {

    @Inject
    private PersonService personService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/add")
    @Retry(retryOn = {IllegalStateException.class})
    @Fallback(fallbackMethod = "addPersonFallback")
    public void add(@Suspended AsyncResponse asyncResponse,
                    @BeanParam @Valid PersonForm personForm) {
        try {
            personService.addPerson(new Person(personForm.getFirstName(), personForm.getLastName()));
            asyncResponse.resume("Person has added successfully");
        } catch (Exception e) {
            throw new IllegalStateException("Exception in adding");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    @Retry(retryOn = {IllegalStateException.class})
    @Fallback(fallbackMethod = "listOfPersonsFallback")
    public void list(@Suspended AsyncResponse asyncResponse) {
        try {
            Set<Person> list = personService.listOfPersons();
            asyncResponse.resume(list);
        } catch (Exception e) {
            throw new IllegalStateException("Exception in getting list");
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete/{id}")
    @Retry(retryOn = {IllegalStateException.class})
    @Fallback(fallbackMethod = "deletePersonFallback")
    public void delete(@Suspended AsyncResponse asyncResponse, @PathParam("id") long id) {
        try {
            personService.deletePerson(personService.findPerson(id));
            asyncResponse.resume("Person has removed successfully");
        } catch (Exception e) {
            throw new IllegalStateException("Exception in deleting");
        }
    }

    private void listOfPersonsFallback(AsyncResponse asyncResponse) {
        asyncResponse.resume("Exception Detected");
    }

    private void deletePersonFallback(AsyncResponse asyncResponse, long id) {
        asyncResponse.resume("Exception Detected");
    }

    private void addPersonFallback(AsyncResponse asyncResponse, PersonForm personForm) {
        asyncResponse.resume("Exception Detected");
    }
}