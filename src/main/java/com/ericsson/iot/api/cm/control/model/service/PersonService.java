package com.ericsson.iot.api.cm.control.model.service;

import com.ericsson.iot.api.cm.control.model.entity.Person;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;


@ApplicationScoped
public class PersonService {

    @Inject
    private EntityManager em;
    private Set<Person> list = new HashSet<>();

    @Transactional
    public void addPerson(Person person) {
        em.persist(person);
    }

    public Set<Person> listOfPersons() {
        list.clear();
        list.addAll(em.createNamedQuery("list.person")
                .getResultList());
        return list;
    }

    public Person findPerson(long id) {
        return (Person) em.createNamedQuery("find.person")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional
    public void deletePerson(Person person) {
        em.remove(em.merge(person));

    }
}
