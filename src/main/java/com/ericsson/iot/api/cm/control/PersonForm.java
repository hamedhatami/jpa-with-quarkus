package com.ericsson.iot.api.cm.control;

import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;

public class PersonForm {

    @FormParam("first_name")
    @NotNull
    private String firstName;
    @FormParam("last_name")
    @NotNull
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
