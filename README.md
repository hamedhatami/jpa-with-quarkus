## Install Postgres

First of all you need to install Postgres as your data storage,  
Hence I suggest you to use dockerized Postgres like below

     $ docker run -d --name postgres -p 5432:5432 -e POSTGRES_DB=hibernate_db -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres postgres
     
You will be able to access to your database  
  
     $ docker exec -it postgres psql -U postgres
     
For switch to your database, you have to use below command

     postgres=# \c hibernate_db
          You are now connected to database "hibernate_db" as user "postgres".
          
     hibernate_db=# select * from person;
     
     
      id | firstname | lastname 
     ----+-----------+----------
       1 | hamed     | hatami
       2 | hamed     | hatami
     (2 rows)


# JPA-with-Quarkus project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application is packageable using `./mvnw package`.
It produces the executable `JPA-with-Quarkus-1.0.0-SNAPSHOT-runner.jar` file in `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/JPA-with-Quarkus-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or you can use Docker to build the native executable using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your binary: `./target/JPA-with-Quarkus-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image-guide .

## Test functionality

You can add a person by below command
    
     curl -X POST "http://localhost:8080/iot/api/person/add" -H "Content-Type: application/x-www-form-urlencoded" --data-urlencode "first_name=hamed" --data-urlencode "last_name=hatami"

You can get the list of persons

     curl -X GET "http://localhost:8080/iot/api/person/list"
     
You can delete a person     

     curl -X DELETE "http://localhost:8080/iot/api/person/delete/3" -H "Content-Type: application/json"